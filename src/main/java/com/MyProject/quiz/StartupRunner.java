package com.MyProject.quiz;

import com.MyProject.quiz.entities.PlayerEntity;
import com.MyProject.quiz.repositories.PlayerRepository;
import com.MyProject.quiz.services.QuizDataService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log
public class StartupRunner implements CommandLineRunner {

    @Autowired
    // Automatically reference to the bean in this field - Injection; @Autowired can only be used in class which is Spring bean = @Component
    private PlayerRepository playerRepository;

    @Autowired
    private QuizDataService quizDataService;

    @Override
    public void run(String... args) {
        log.info("Executing startup actions...");
        playerRepository.save(new PlayerEntity("Petr"));
        playerRepository.save(new PlayerEntity("Tomas"));
        playerRepository.save(new PlayerEntity("David"));

        quizDataService.getQuizCategories();

        log.info("List of players from db:");
        List<PlayerEntity> playersFromDatabase = playerRepository.findAll();
        for (PlayerEntity player : playersFromDatabase) {
            log.info("Player from DB:" + player);
        }
    }
}
