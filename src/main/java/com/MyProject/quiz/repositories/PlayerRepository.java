package com.MyProject.quiz.repositories;

import com.MyProject.quiz.entities.PlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<PlayerEntity, Long> {
    // by generics we give PlayerEntity - used in this repository; Long - identifier (@Id)
}
