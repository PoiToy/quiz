package com.MyProject.quiz.services;
import com.MyProject.quiz.QuizApplication;
import com.MyProject.quiz.dto.CategoryQuestionCountInfoDto;
import com.MyProject.quiz.dto.QuestionsDto;
import com.MyProject.quiz.frontend.Difficulty;
import com.MyProject.quiz.frontend.GameOptions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

//TODO: this tests doesn´t work
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {QuizApplication.class})
@ExtendWith(MockitoExtension.class)
class QuizDataServiceTest {

    @Autowired
    QuizDataService service;

    @Mock
    RestTemplate template;

    //@Test
    void getQuizQuestions() {
        GameOptions go = new GameOptions();
        go.setCategoryId(9);
        go.setNumberOfQuestions(2);
        go.setDifficulty(Difficulty.MEDIUM);
        CategoryQuestionCountInfoDto result = new CategoryQuestionCountInfoDto();

        try (MockedConstruction<RestTemplate> mocked = Mockito.mockConstruction(RestTemplate.class,
                (mock, context) -> {
                    // further stubbings ...
                    when(mock.getForObject(anyString(), CategoryQuestionCountInfoDto.class)).thenReturn(result);
                })) {
            List<QuestionsDto.QuestionDto> quizQuestions = service.getQuizQuestions(go);
            assertEquals(quizQuestions, result);
        }
    }

    // @Test
    void getQuizQuestionsAutowiredTemplate() {
        GameOptions go = new GameOptions();
        go.setCategoryId(9);
        go.setNumberOfQuestions(2);
        go.setDifficulty(Difficulty.MEDIUM);
        CategoryQuestionCountInfoDto result = new CategoryQuestionCountInfoDto(new CategoryQuestionCountInfoDto());

        service.setRestTemplate(template);
        reset(template);
        //CategoryQuestionCountInfoDto result = restTemplate.getForObject(uri, CategoryQuestionCountInfoDto.class);
//        when(template.getForObject(any(), any(CategoryQuestionCountInfoDto.class)))
//                .thenReturn(result);


        List<QuestionsDto.QuestionDto> quizQuestions = service.getQuizQuestions(go);
        assertEquals(result, quizQuestions);

    }
}
