package com.MyProject.quiz;
import com.MyProject.quiz.entities.PlayerEntity;
import com.MyProject.quiz.repositories.PlayerRepository;
import com.MyProject.quiz.services.QuizDataService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {QuizApplication.class})
@ExtendWith(MockitoExtension.class)
class StartupRunnerTest {

    @Autowired
    StartupRunner runner;

    @SpyBean
    QuizDataService serv;

    @MockBean
    private PlayerRepository playerRepository;

    @Test
    void run() {
        assertNotNull(playerRepository);
        ArgumentCaptor<PlayerEntity> plName = ArgumentCaptor.forClass(PlayerEntity.class);
        List<PlayerEntity> playerEntityList = new ArrayList<>();
        when(playerRepository.save(plName.capture())).thenAnswer(invocationOnMock -> {
            playerEntityList.add(plName.getValue());
            return null;

        });

        when(playerRepository.findAll()).thenReturn(playerEntityList);


        runner.run();

        verify(playerRepository,times(3)).save(any());

        verify(serv,times(1)).getQuizCategories();
        assertEquals("David", plName.getValue().getName());
    }
}

